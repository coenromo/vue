# Entain technical task

To run locally

1. `cd state`
2. `npm i`
3. `npm run serve`

or

1. `npm run build`
2. `serve -s dist`
   - -s flag means serve it in Single-Page Application mode

# Unit tests

1. `npm run test:unit`
   - Only created very basic ones, should definitely have tried to mock data and test the `v-for` iterations
2. `$(npm bin)/cypress run`
   - Just check data exists and quit essentially, more of a ping test

# App.ts

Main source file for app

- Uses axios to perform fetch request
- Re maps element keys to 0, 1, 2, etc so the table can show max rows of 5
- Assigns "Start Time" and "Countdown Time" to each race
- Maps `this.dataResponse` to component using `v-for`
- Checks for 60s past start time and removes item from array
- Filters and Sorts on new button click

Only major issue here is the re-fetch on click, causing the table to flicker and just generally not be the best practice. My thinking was that we wouldn't know when the last time the user had performed an action on the page, so we should just re-fetch to always show the latest data. But now I think I should have checked for the last race start time to get an idea of when to re-fetch so it is done in the background. That and obviously moving this into state and using getters and mutations, but I ran out of time looking at the doco to get that implemented after the initial barebones app was created.

# ClockCountDown

- Countdown component used to display time to race start
- Displays "timeLeft" or "Race Started"

# Response

- ("Tab ¿ Long May We Play") exists in "Albion Park" race response
- Haven't seen it since so maybe fixed, or maybe "Albion Park" race hasn't come up since then... is this the legit API used for the apps?
