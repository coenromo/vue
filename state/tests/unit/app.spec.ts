import RacingApp from "@/components/home/RacingApp.vue";
import { shallowMount } from "@vue/test-utils";

// check initial render of component
describe("RacingApp.vue", () => {
  it("renders props.msg when passed", () => {
    const testMsg =
      "Next to Jump Races Racing  Greyhound  Harness  Horse #Meeting NameRace NumberStart TimeTimer";
    const wrapper = shallowMount(RacingApp, {
      props: { testMsg },
    });
    expect(wrapper.text()).toMatch(testMsg);
  });
});
