import { shallowMount } from "@vue/test-utils";
import ClockCountDown from "@/components/clock-countdown/ClockCountDown.vue";

describe("Clock counter", () => {
  it("renders second countdown when passed", () => {
    const timeLeft = 20;
    const wrapper = shallowMount(ClockCountDown, {
      props: { timeLeft },
    });
    expect(wrapper.text()).toEqual(`${timeLeft}s`);
  });
  it("renders !!! Race Started !!!! when time up", () => {
    const timeLeft = -65;
    const wrapper = shallowMount(ClockCountDown, {
      props: { timeLeft },
    });
    expect(wrapper.text()).toEqual(`!!! Race Started !!! ${timeLeft}`);
  });
});
