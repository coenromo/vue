import { shallowMount } from "@vue/test-utils";
import Clock from "@/views/Clock.vue";

describe("Clock Counter", () => {
  it("renders timer when passed", () => {
    const msg = "30";
    const wrapper = shallowMount(Clock, {
      props: { msg },
    });
    expect(wrapper.text()).toEqual(msg);
  });
});
