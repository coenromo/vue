import { shallowMount } from "@vue/test-utils";
import RacingApp from "@/components/home/RacingApp.vue";

describe("Main app spec", () => {
  it("renders page title", () => {
    const msg =
      "Next to Jump Races Racing  Greyhound  Harness  Horse #Meeting NameRace NumberStart TimeTimer";
    const wrapper = shallowMount(RacingApp, {
      props: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});

// should do some v-for tests with mock data here
