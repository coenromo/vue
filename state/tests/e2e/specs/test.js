// https://docs.cypress.io/api/introduction/api.html

describe("My First Test", () => {
  it("Visits the app root url", () => {
    cy.visit("http://localhost:8080/");
  });
  it("check h3", () => {
    cy.get("h3").contains("Next to Jump Races");
  });
  it("checks button", () => {
    cy.get('[id^="9daef0d7-bf3c-4f50-921d-8e818c60fe61"]').contains(
      "Greyhound"
    );
  });
  it("checks table length", () => {
    cy.get(".table").length > 0;
  });
});
