import { createStore } from "vuex";
import axios from "axios";

/* eslint-disable */

// to handle state
const state = {
  data: [],
  isBusy: false,
};

// to handle state
const getters = {};

// to handle actions
/**
 * @action
 * @returns API response
 * perform fetch
 * I started to look into vuex management after the initial app was working
 * Just ran out of time to get a fully working implementation
 */
const actions = {
  getData({ commit }: any) {
    axios
      .get("https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=10")
      .then((response) => {
        // re-assign keys to 0,1,2 etc
        const reArray: Array<string> = [];
        for (const key in response.data.data.race_summaries) {
          if (
            Object.prototype.hasOwnProperty.call(
              response.data.data.race_summaries,
              key
            )
          ) {
            const element = response.data.data.race_summaries[key];

            // push to array
            reArray.push(element);
          }
        }
        // commit data with new mapped keys
        commit("SET_DATA", reArray);
      });
  },
};

// to handle mutations
const mutations = {
  SET_DATA(state: any, data: any) {
    state.data = data;
  },
};

//export store module
export default createStore({
  state,
  getters,
  actions,
  mutations,
});
