import { Vue, Options } from "vue-class-component";
import axios from "axios";
import ClockCounter from "../clock/ClockCounter.vue";
import ClockCountDown from "../clock-countdown/ClockCountDown.vue";

// load components
@Options({
  components: {
    ClockCounter,
    ClockCountDown,
  },
})
export default class RacingApp extends Vue {
  mounted() {
    // fetch state
    // this.$store.dispatch("getData");
  }

  /**
   * @trigger every second
   * loop through array and splice if 1 minute past start time has expired
   * this ideally would be moved to state
   */
  getNewData() {
    // get current time, +1 hour for AEST, plus 60seconds for race disappear time
    const currentEpoch = Math.floor(Date.now() / 1000) - 60;
    for (const iterator of this.dataResponse) {
      const convertedEpochRaceTime = iterator.advertised_start.seconds;
      // if equal to current time or less, remove
      if (
        convertedEpochRaceTime === currentEpoch ||
        convertedEpochRaceTime <= currentEpoch
      ) {
        this.dataResponse.splice(0, 1);
      }
    }
  }

  /**
   * declare main variables
   */
  // error response
  errors: any[];
  // main data response
  dataResponse: any[];
  // interval used for timer
  interval: number;
  // raceType used on home page
  raceType = "Races";

  /**
   * button click event
   * @param id button ID
   * returns sorted array, filtered by category
   * performs a new fetch, as not sure how long the user has been sitting on home page
   * TODO - would move this into state management so it wasn't so crude
   * Probably could have checked for latest start time, and only re-fetch when we are approaching that
   */
  async onClick(id: { target: { innerHTML: string; id: string } }) {
    // set h1 text as clicked button text
    this.raceType = id.target.innerHTML;

    // set data as empty and re-fetch
    this.dataResponse = [];
    await this.fetchData();

    // if 'racing' clicked we can just reset seconds sort
    if (id.target.id === "reset") {
      // re-sort by start time
      this.dataResponse.sort(
        (
          firstParam: { advertised_start: { seconds: number } },
          secondParam: { advertised_start: { seconds: number } }
        ) =>
          firstParam.advertised_start.seconds >
          secondParam.advertised_start.seconds
            ? 1
            : -1
      );
    }

    // refetch and filter for button category
    if (id.target.id !== "reset") {
      // map only those category ID back to list
      const mappedList = this.dataResponse
        .map((item: { category_id: string }) => {
          if (item.category_id === id.target.id) {
            return item;
          }
          return;
        })
        // filter out nulls
        .filter((nulls: any) => nulls)
        // sort by advertised_start time
        .sort((firstParam: any, secondParam: any) =>
          firstParam.advertised_start.seconds >
          secondParam.advertised_start.seconds
            ? 1
            : -1
        );
      // assign back to dataResponse so table can update
      // as above, would ideally move this to state mutations
      this.dataResponse = mappedList;
    }
  }

  // data
  data() {
    return {
      errors: [],
      dataResponse: [],
      ClockCounter,
    };
  }

  /**
   * fetch data from API
   * @fetch
   * @returns data mapped to new keys
   */
  async fetchData() {
    try {
      const response = await axios.get(
        "https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=100"
      );

      // assign response to variable to iterate over and for readability
      const hashedKey = response.data.data.race_summaries;

      // re-assign keys to 0,1,2 etc to only display 5 races
      for (const key in hashedKey) {
        if (Object.prototype.hasOwnProperty.call(hashedKey, key)) {
          const element = hashedKey[key];
          this.dataResponse.push(element);
        }
      }

      // map start time to data
      /**
       * not technically mutating data, just re-assigning to new keys :)
       * I guess I could have done this on the fly while leaving the response as an immutable object
       */
      this.dataResponse.map(
        (item: {
          advertised_start: { seconds: number };
          startTime: string;
          countdownTime: number;
        }) => {
          // create new date in local timezone
          const dateReturn = new Date(
            item.advertised_start.seconds * 1000
          ).toLocaleTimeString("en-AU", { timeZone: "Australia/Brisbane" });

          // assign in date format to display Start Time
          item.startTime = dateReturn;
          // create to display on countdown timer in seconds
          item.countdownTime =
            item.advertised_start.seconds - Math.floor(Date.now() / 1000);
        }
      );

      // sort by advertised start time
      this.dataResponse.sort(
        (
          a: { advertised_start: { seconds: number } },
          b: { advertised_start: { seconds: number } }
        ) => (a.advertised_start.seconds > b.advertised_start.seconds ? 1 : -1)
      );
    } catch (error) {
      this.errors.push(error);
    }
  }

  // called synchronously after the instance is created
  async created() {
    // check currentTime every second to remove race start from list
    this.interval = setInterval(() => this.getNewData(), 1000);
    // fetch data on load
    this.fetchData();
  }
}
