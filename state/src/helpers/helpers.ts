/**
 * helper functions to get current formatted date
 */
export function currentTime(): number {
  return Math.floor(Date.now());
}

export const currentEpoch = Math.floor(Date.now() / 1000);
